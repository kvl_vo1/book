import './style/style.scss';

$(document).ready(function () {
  let owl = $('.owl-carousel');

  owl.owlCarousel({
    items: 1,
    loop: true,
    dots: false,
    nav: false,
    autoplay: true,
    autoplayTimeout: 3000
  });

  $('.slider-wrapper__next').click(function() {
    owl.trigger('next.owl.carousel');
  });

  $('.slider-wrapper__prev').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
  })
});